<?php

namespace yunj\test;

class RemoveVenderGitDir {

    public static function postInstall() {
        self::del();
    }

    public static function postUpdate() {
        self::del();
    }

    public static function del() {
        defined("DS") ?: define("DS", DIRECTORY_SEPARATOR);
        $baseDir = __DIR__ . DS . ".." . DS . "vender" . DS . "yunj" . DS;
        $baseDirFiles = scandir($baseDir);
        $gitDirs = [];
        foreach ($baseDirFiles as $baseDirFile) {
            if ($baseDirFile == "." || $baseDirFile == "..") continue;
            $path = $baseDir.$baseDirFile.DS;
            if(is_dir($path)) $gitDirs = array_merge($gitDirs,self::findGitDir($path));
        }
        if(!$gitDirs) return;
        if(strpos(strtoupper(PHP_OS),"WIN")!==false)
            foreach ($gitDirs as $gitDir) exec("rmdir /s /q ".$gitDir);
        else
            foreach ($gitDirs as $gitDir) exec("rm -rf ".$gitDir);

    }

    private static function findGitDir($dir) {
        $gitDirs = [];
        $items = scandir($dir);
        foreach ($items as $item) {
            if ($item == "." || $item == ".." || !is_dir($dir.$item)) continue;
            if($item==".git") $gitDirs[] = $dir.$item;
        }
        return $gitDirs;
    }

}